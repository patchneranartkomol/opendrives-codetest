# Factoid Backend
This is a backend REST API, backed by an in-memory SQLite database that is seeded and refreshed each time the server is restarted.

## Available Scripts

In the `backend` project directory, you can run:

### `npm start`

Runs the app.

### Endpoints

`GET /factoids`

Open [http://localhost:8000/factoids](http://localhost:8000/factoids) to view the response in your browser.

`POST /factoids`

Test this endpoint with cURL:
```
 curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"description":"test","factoid":"test","image_url":"test"}' \
  http://localhost:8000/factoids
```

`DELETE /factoids/:id`

Test this endpoint with cURL:
```
curl -v -X DELETE http://localhost:8000/factoids/1
```

### Linting

Run `npm run lint`

### Testing

There are currently no unit tests. To run the integration test suite, start the server, then run `npm test` in another terminal.

### Deployment

* SSH onto the host server.
* Run `git pull` if the code on the server is not up-to-date.
* Run `export NODE_ENV=production; npm start`
