exports.seed_data = [
  {
    $description: 'New York Knicks',
    $factoid: 'The Knicks last NBA championship was in 1973.',
    $image_url: 'https://cdn.nba.com/logos/nba/1610612752/primary/L/logo.svg',
  },
  {
    $description: 'Los Angeles Lakers',
    $factoid: "The Lakers hold the record for NBA's longest winning streak, 33 straight games, set during the 1971–72 season.",
    $image_url: 'https://cdn.nba.com/logos/nba/1610612747/primary/L/logo.svg',
  },
  {
    $description: 'Golden State Warriors',
    $factoid: '2022 NBA Champs',
    $image_url: 'https://cdn.nba.com/logos/nba/1610612744/primary/L/logo.svg',
  },
  {
    $description: 'Philadelphia 76ers',
    $factoid: "The team's recent slogan was Trust The Process.",
    $image_url: 'https://cdn.nba.com/logos/nba/1610612755/primary/L/logo.svg',
  },
  {
    $description: 'Chicago Bulls',
    $factoid: "The Bulls are known for having one of the NBA's greatest dynasties, winning six NBA championships between 1991 and 1998 with two three-peats.",
    $image_url: 'https://cdn.nba.com/logos/nba/1610612741/primary/L/logo.svg',
  },
  {
    $description: 'New York Knicks',
    $factoid: 'Jason Concepcion, host of the Takeline podcast, is a fan.',
    $image_url: 'https://cdn.nba.com/logos/nba/1610612752/primary/L/logo.svg',
  },
  {
    $description: 'Los Angeles Lakers',
    $factoid: 'Home stadium is the Crypto.com Arena.',
    $image_url: 'https://cdn.nba.com/logos/nba/1610612747/primary/L/logo.svg',
  },
];
