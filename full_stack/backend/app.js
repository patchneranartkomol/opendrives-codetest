const express = require('express');
const sqlite3 = require('sqlite3').verbose();
const path = require('path');
const cors = require('cors');

const seed = require('./seed_data');

const app = express();
const db = new sqlite3.Database(':memory:'); // In-memory database will be wiped each time server is restarted
const port = (process.env.NODE_ENV === 'production') ? 80 : 8000;

app.use(express.json());
app.use(express.static(path.join(__dirname, '../frontend/build')));
app.use(cors());

// This is a hack to host the built frontend from the same backend API
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '../frontend/build', 'index.html'));
});

app.get('/factoids', (req, res) => {
  // SQLite tables implicitly contain rowid by default
  db.all('SELECT rowid, description, factoid, image_url FROM factoids', (err, rows) => {
    if (err) {
      console.log(err);
      res.status(400).send(err);
    }
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(rows));
  });
});

app.delete('/factoids/:id', (req, res) => {
  // SQLite tables implicitly contain rowid by default
  db.run('DELETE FROM factoids WHERE rowid = ?', [req.params.id], (err) => {
    if (err) {
      console.log(err);
      res.status(400).send(err);
    }
    res.status(204).send();
  });
});

app.post('/factoids', (req, res) => {
  db.run('INSERT INTO factoids VALUES (?, ?, ?)', [req.body.description, req.body.factoid, req.body.image_url], (err) => {
    if (err) {
      console.log(err);
      res.status(400).send(err);
    }
    res.status(204).send();
  });
});

app.listen(port, () => {
  db.serialize(() => {
    db.run('CREATE TABLE factoids (description TEXT, factoid TEXT, image_url TEXT)');
    console.log('Created in-memory database');

    seed.seed_data.forEach((r) => {
      db.run('INSERT INTO factoids VALUES ($description, $factoid, $image_url)', r);
    });
    console.log('Seeding in-memory database with factoids');
  });

  console.log(`App listening on port ${port}`);
});
