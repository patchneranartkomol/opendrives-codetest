const expect = require('chai').expect
const request = require('request')

describe("Factoid API Integration tests",() => {
  describe("GET Endpoint", () => {
    const url = "http://localhost:8000/factoids";
    it("returns status 200", () => {
      request(url, (error, response, body) => {
        expect(response.statusCode).to.equal(200);
      });
    });
  });

  describe("DELETE Endpoint", () => {
    const url = "http://localhost:8000/factoids/1";
    it("returns status 204", () => {
      request.del(url, (error, response, body) => {
        expect(response.statusCode).to.equal(204);
      });
    });
  });

  describe("POST Endpoint", () => {
    const url = "http://localhost:8000/factoids";
    const reqBody = {factoid: "test",
                     description: "test",
                     image_url: "test"};
    it("returns status 204", () => {
      request.post({url: url,
        json: reqBody},(error, response, body) => {
        expect(response.statusCode).to.equal(204);
      });
    });
  });

});
