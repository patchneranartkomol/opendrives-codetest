import React, { useState } from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import DialogTitle from '@mui/material/DialogTitle';
import Grid from '@mui/material/Grid';
import AddIcon from '@mui/icons-material/Add';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import SendIcon from '@mui/icons-material/Send';
import Modal from '@mui/material/Modal';
import SkipPreviousIcon from '@mui/icons-material/SkipPrevious';
import SkipNextIcon from '@mui/icons-material/SkipNext';
import Typography from '@mui/material/Typography';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import TextField from '@mui/material/TextField';

import { useAPI } from "./models/APIContext";
import FactoidCard from "./FactoidCard"
import teamMap from "./teamImgMapping"

export default function Factoids(props) {
  // Display card state
  const { factoids, deleteFactoid, createFactoid } = useAPI();
  const [index, setIndex] = useState(0);

  // Creation modal state
  const [modalOpen, setModalOpen] = useState(false);
  const handleModalOpen = () => setModalOpen(true);
  const handleModalClose = () => setModalOpen(false);
  const [selectedTeam, setSelectedTeam] = useState('Los Angeles Lakers');
  const handleSelectedTeam = (e) => setSelectedTeam(e.target.value);
  const [newfactoidText, setNewFactoidText] = useState('');
  const handleFactoidText = (e) => setNewFactoidText(e.target.value);
  const teamImgMapping = teamMap.teamImgMapping;

  const modalStyle = {
      position: 'absolute',
      top: '50%',
      left: '50%',
      fontFamily: '"Roboto","Helvetica","Arial",sans-serif',
      transform: 'translate(-50%, -50%)',
      width: 400,
      bgcolor: 'background.paper',
      border: '1px outset #000',
      boxShadow: 24,
      p: 4,
  };


  // Render a loading screen while browser fetches factoids from backend
  if (factoids[index] === undefined) {
    return (<div>loading</div>);
  }

  const handleNext = (event) => {
    if (index === factoids.length - 1)
      setIndex(0);
    else
      setIndex(index + 1);
  }
  const handlePrevious = (event) => {
    if (index === 0)
      setIndex(factoids.length - 1);
    else
      setIndex(index - 1);
  }

  const handleDelete = (event) => {
    deleteFactoid(factoids[index].rowid);
    if (index > 0)
      setIndex(index - 1);
  }

  const handleSubmitCreate = (event) => {
    setModalOpen(false);
    createFactoid(selectedTeam, newfactoidText, teamImgMapping[selectedTeam]);

    setSelectedTeam('Los Angeles Lakers');
    setNewFactoidText('');
  }

  return (
    <Grid
      container
      spacing={0}
      direction="column"
      alignItems="center"
      justifyContent="center"
      style={{ minHeight: '100vh' }}
    >
      { /* Current card user is interacting with should be displayed here.  */ }
      <FactoidCard description={factoids[index].description}
                   imageUrl={factoids[index].image_url}
                   factoid={factoids[index].factoid}
      />
      <ButtonGroup variant="contained"
                   aria-label="outlined primary button group">
        <Button aria-label="next" startIcon={<SkipPreviousIcon />} onClick={handlePrevious}/>
        <Button startIcon={<AddIcon />} onClick={handleModalOpen}>Create</Button>
        { /* Edit not currently implemented */ }
        <Button startIcon={<EditIcon />}>Edit</Button>
        <Button startIcon={<DeleteIcon />} onClick={handleDelete}>Delete</Button>
        <Button aria-label="next" endIcon={<SkipNextIcon />} onClick={handleNext}/>
      </ButtonGroup>

      { /* Creation popover modal. Only displayed when user clicks Create */ }
      <Modal
      open={modalOpen}
      onClose={handleModalClose}
      >
        <Box sx={{ ...modalStyle, width: 400, minHeight: 400 }}>
            <DialogTitle>Create a new factoid</DialogTitle>
            <Box sx={{ minWidth: 120 }}>
              <FormControl fullWidth>
                <InputLabel>NBA Team</InputLabel>
                <Select
                value={selectedTeam}
                label="NBA Team"
                onChange={handleSelectedTeam}
                >
                  <MenuItem value={"Los Angeles Lakers"}>Los Angeles Lakers</MenuItem>
                  <MenuItem value={"Philadelphia 76ers"}>Philadelphia 76ers</MenuItem>
                  <MenuItem value={"Chicago Bulls"}>Chicago Bulls</MenuItem>
                  <MenuItem value={"Golden State Warriors"}>Golden State Warriors</MenuItem>
                  <MenuItem value={"New York Knicks"}>New York Knicks</MenuItem>
                </Select>
                <img
                  width="200"
                  height="200"
                  src={teamImgMapping[selectedTeam]}
                  alt={selectedTeam}
                />

                <Typography sx={{ padding: 1 }} >
                  Enter your factoid below.
                </Typography>
                <TextField variant="outlined" value={newfactoidText} onChange={handleFactoidText}/>

                <Button startIcon={<SendIcon />}variant="contained" onClick={handleSubmitCreate}>Submit</Button>
              </FormControl>
            </Box>
        </Box>
    </Modal>
    </Grid>
  );
}
