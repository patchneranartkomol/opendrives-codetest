import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';

export default function FactoidCard(props) {
  return (
      <Card sx={{ minWidth: 432,
                  maxWidth: 432, }}>
        <CardContent>
          <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
            {props.description}
          </Typography>
          <img
            width="200"
            height="200"
            src={props.imageUrl}
            alt={props.description}
          />
          <Typography variant="body2">
            {props.factoid}
          </Typography>
        </CardContent>
      </Card>
  );
}
