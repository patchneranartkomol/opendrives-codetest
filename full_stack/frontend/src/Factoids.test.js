import { render, screen } from '@testing-library/react';
import Factoids from './Factoids';

import { APIContextProvider } from "./models/APIContext";

test('renders loading screen on initial render', () => {
  // Arrange
  // Act
  render(<APIContextProvider>
          <Factoids />
         </APIContextProvider>);
  const el = screen.getByText(/loading/i);
  // Assert
  expect(el).toBeInTheDocument();
});
