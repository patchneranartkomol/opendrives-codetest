import { render, screen } from '@testing-library/react';
import App from './App';

test('renders a loading message when first rendered', () => {
  // Arrange
  // Act
  render(<App />);
  const el = screen.getByText(/loading/i);

  // Assert
  expect(el).toBeInTheDocument();
});
