import { APIContextProvider } from "./models/APIContext";
import Factoids from "./Factoids";

function App() {
  return (
      <div className="App">
        <APIContextProvider>
          <Factoids/>
        </APIContextProvider>
      </div>
  );
}

export default App;
