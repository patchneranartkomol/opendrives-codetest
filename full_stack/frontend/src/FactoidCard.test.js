import { render, screen } from '@testing-library/react';
import FactoidCard from './FactoidCard';

test('renders description and factoid', () => {
  // Arrange
  render(<FactoidCard description={'testDescription'}
                      factoid={'testFactoid'}
                      imageUrl={'testImageUrl'}/>);
  const description = screen.getByText(/testDescription/i);
  const factoid = screen.getByText(/testDescription/i);

  // Assert
  expect(description).toBeInTheDocument();
  expect(factoid).toBeInTheDocument();
});
