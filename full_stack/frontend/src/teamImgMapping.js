const teamImgMapping = {
  teamImgMapping: {
  'New York Knicks': 'https://cdn.nba.com/logos/nba/1610612752/primary/L/logo.svg',
  'Los Angeles Lakers': 'https://cdn.nba.com/logos/nba/1610612747/primary/L/logo.svg',
  'Golden State Warriors': 'https://cdn.nba.com/logos/nba/1610612744/primary/L/logo.svg',
  'Philadelphia 76ers': 'https://cdn.nba.com/logos/nba/1610612755/primary/L/logo.svg',
  'Chicago Bulls': 'https://cdn.nba.com/logos/nba/1610612741/primary/L/logo.svg'
  }
};

export default teamImgMapping;
