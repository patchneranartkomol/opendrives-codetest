import React, { useContext, useState, useEffect, createContext } from 'react';
import axios from 'axios';

const APIContext = createContext();

// Include prod URL when building for Prod
const APIUrl = (process.env.NODE_ENV === 'production') ? 'http://164.90.155.45/factoids': 'http://localhost:8000/factoids'

export function APIContextProvider({ children }) {
  const [factoids, setFactoids] = useState([]);

  async function fetchData() {
    const { data } = await axios.get(APIUrl);
    setFactoids(data);
  }

  function deleteFactoid(id) {
    setFactoids(factoids.filter((fObj) => fObj.rowid !== id));
    axios.delete(`${APIUrl}/${id}`)
      .then(function (response) {
            console.log(response);
          })
      .catch(function (error) {
            console.log(error);
          });
  }

  function createFactoid(selectedTeam, newfactoidText, image_url) {
    axios.post(APIUrl, {
          description: selectedTeam,
          factoid: newfactoidText,
          image_url: image_url
        })
      .then(function (response) {
            console.log(response);
          })
      .catch(function (error) {
            console.log(error);
          });
    fetchData();
  }


  useEffect(() => {
    fetchData();
  }, []);

  return (
    <APIContext.Provider
      value={{
        factoids,
        deleteFactoid,
        createFactoid
      }}
    >
      {children}
    </APIContext.Provider>
  );
}

export function useAPI() {
  const context = useContext(APIContext);
    if (context === undefined) {
      throw new Error("Context must be used within APIContextProvider");
    }
  return context;
}
