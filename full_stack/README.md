# Full Stack Code Test Submission - NBA Team Factoid Cards

The factoids presented are about a few teams in the NBA, or National Basketball Association. I've only added 5 teams into the list of facts and drop-downs, rather than all 30 teams in the league just for compactness.

The user can cycle through the factoids to view next or previous cards, create, or delete existing cards. The edit functionality is not implemented, at the moment.

## App Demo

View a running example [here](http://164.90.155.45/) deployed to a cloud hosting provider, in this case, a Digital Ocean Droplet.

NOTE - I'm just running `node app.js` on port 80, instead of setting up NGINX as reverse proxy, daemonizing the node process, etc, so the app may have avaibility issues. If the example app is not currently running, please ping me to restart the server.

#### Main Screen
The user can interact with factoids, scroll through the list of factoids.
![main screen](main_screen.png "Factoid Card Main Screen")

#### Factoid Creation Modal
Clicking create opens a modal that lets the user create their own factoids, selecting from a drop-down of specific teams.
![create screen](create_screen.png "Factoid Create Screen")

## Running the code locally

There are separate `/frontend` and `/backend` folders. To run the project locally, we just need to install node modules for each, and run both projects:

In one terminal:
```
cd frontend
npm i
npm start
```

In another terminal:
```
cd backend
npm i
npm start
```

See more details about running locally and deployment in each respective folder's README.

# Full Stack Code Test: 'Factoid Cards'

*Please read this whole README before starting.*

Welcome to the full stack engineer code test! This will be a test of your:
1. Ability to use standard full stack tools
2. Ability to troubleshoot challenging issues
3. Capability to organize, prioritize, and execute

The point of this test is to understand how you approach a technical problem. The way you code is just as important as your solution. 

*Please communicate with us via email as if you already work here, and add documentation/comments as you go along.*

We expect this test to take you about *6 - 10 hours*. Given that most people are busy, We want to hear back from you *within 72 hours of starting this test*.

Send us an email when you're finished and we'll schedule a code review with engineers from across our teams to discuss your submission. We'll discuss the finer
 points of your implementation, walk through your code, demo its functionality (or troubleshoot its brokenness), and give feedback on its strengths, weaknesses 
 and, if we're being honest with ourselves, bugs. We don't expect your submission to be perfect, but we do expect you to be able to frankly communicate and 
 discuss your submission in a professional environment.

## Overall Requirements
Create an application to feature one of your extracurricular interests. Select an interest that has items of alike members (Stamps, Books, Songs, etc). Please use this as a chance to showcase your personality by selecting a category of things you really enjoy, perhaps things you do for fun.

The application should have a frontend for a web browser and a backend in NodeJs, use whatever libraries you are most familiar with otherwise.

#### As soon as you begin:
* [x] Fork this repository into a public repository on your gitlab/etc account

#### While working on this, please:
* [x] Commit early and often. We'll likely be following along with your progress.

#### Upon completing this, please email to us:
* [x] A link to your git repository such that we may view your code. - https://gitlab.com/patchneranartkomol/opendrives-codetest/-/tree/master/full_stack
* [x] A link to somewhere where we may interact with a demo of your finished product - http://164.90.155.45/

## Front End Requirements

The User Interface should be made out of a MVVM or MVC style frontend technology, using an asynchronous transport technology to talk to your backend.

#### The user interface should present a card like entry containing
* [x] A small picture of the item
* [x] A description of the item
* [x] A fun factoid for the item

#### Interactions
* [x] The user should be able to dismiss the currently presented card, making it disappear and presenting the next card in the stack. (Think of swiping in Tinder)
* [x] The user should be able to create their own factoid to be added to the fact stack.
* [x] The user should be able to remove a factoid from the fact stack.

## Backend Requirements

The Backend should be NodeJs with whatever middleware(s) you'd like to use.

#### Interactions to support
* [x] Please provide a RESTful backend for the CRUDL of factoids
* [x] Factoids should be transported to the UI via JSON
* [x] Factoids should be stored in your database on the backend. You can use any database you want: SQL, NoSQL, or flat files such as text files (.txt)!

## BONUS

Consider adding some improvements to what you've made to really showcase your strengths.

### Suggestions for improvements
* [ ] Integrate this with an external Service or API (Pokemon API FTW)
* [ ] Make it work on Mobile device sizes.
* [x] Write a test or two for your code.
* [x] Add a lint file and make sure your code is linted.
* [ ] Add CD/CI to your project to auto-deploy it
* [ ] Edit this ReadMe with new suggestions for how to improve this code test

## Closing Thoughts

Every developer on our team has completed this code challenge or one of the other code challenges here, so show us what you can do! We're excited to see what kind of contributions you could make to our team.
